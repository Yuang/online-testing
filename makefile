all: compile-student apply-student compile-admin apply-admin build

hello:
	echo "hello"

yes:
	echo "yes"

vueDir=./source/vue
studentDir=${vueDir}/xzs-student
studentRes=${studentDir}/student
adminDir=${vueDir}/xzs-admin
adminRes=${adminDir}/admin
serverDir=./source/xzs
serverStatic=${serverDir}/src/main/resources/static

compile-student:
	cd ${studentDir} && pwd && npm run-script build --mode dev

apply-student:
	rm -rf ${serverStatic}/student
	cp -r ${studentRes} ${serverStatic}

compile-admin:
	cd ${adminDir} && pwd && npm run-script build --mode dev

apply-admin:
	rm -rf ${serverStatic}/admin
	cp -r ${adminRes} ${serverStatic}

build: 
	cd ${serverDir} && mvn compile

clean:
	echo "clean unwritten"
